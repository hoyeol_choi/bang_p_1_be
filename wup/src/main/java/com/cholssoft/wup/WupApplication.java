package com.cholssoft.wup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WupApplication {

	public static void main(String[] args) {
		SpringApplication.run(WupApplication.class, args);
	}

}
